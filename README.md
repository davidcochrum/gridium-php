# David Cochrum's Gridium Test

#### Showcasing my ability to scrape data from HTML tables and parse it into meaningful information.

I wasn't sure which "tide forecast page" the instructions referred to between the "Tide Times" or the "7 Day Tide Table"
so I went ahead and did both.

I'm most comfortable and fluent in PHP therefore these implementations are done in that language. I found a library on
GitHub called [Goutte](https://github.com/FriendsOfPHP/Goutte) (it's mostly a thin wrapper around
[Symfony's Dom Crawler](https://github.com/symfony/dom-crawler)) to do the dirty work of setting up the cURL connections
and parsing the HTML.

In order to best serve the reviewer, I decided to add a subdomain and spin up a DigitalOcean instance to host
the test files to enable rendering the results in the browser.

I took the liberty of applying Bootstrap 4 styles and FontAwesome icons to improve the curb appeal a bit.

## Tide Times Implementation:

Result: [https://gridium-php.dcochrum.com/](https://gridium-php.dcochrum.com/)

Source: [https://gitlab.com/davidcochrum/gridium-php/blob/master/index.php](https://gitlab.com/davidcochrum/gridium-php/blob/master/index.php)

## 7 Day Tide Table Implementation

Result: [https://gridium-php.dcochrum.com/seven-day.php](https://gridium-php.dcochrum.com/seven-day.php)

Source: [https://gitlab.com/davidcochrum/gridium-php/blob/master/seven-day.php](https://gitlab.com/davidcochrum/gridium-php/blob/master/seven-day.php)
