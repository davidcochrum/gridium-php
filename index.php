<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 *
 * Showcasing my ability to scrape data from HTML tables and parse it into meaningful information.
 *
 * This implementation parses the "Tide Times" views for each of the requested cities:
 * - Half Moon Bay, California
 * - Huntington Beach, California
 * - Providence, Rhode Island
 * - Wrightsville Beach, North Carolina
 */

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

include_once 'vendor/autoload.php';

// Set the URL pattern with a placeholder for the location slug.
$urlPattern = 'https://www.tide-forecast.com/locations/{location}/tides/latest';

// Import the list of data for each location.
include_once '_locations.php';

$title = "David Cochrum's Gridium Test";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <title><?= $title ?></title>
</head>
<body>
<main role="main" class="container">
    <div class="jumbotron">
        <img class="img-thumbnail float-right" src="my-pretty-face.jpg" alt="My pretty face" style="max-width: 7rem;">

        <h1><?= $title ?></h1>

        <p class="lead">Showcasing my ability to scrape data from HTML tables and parse it into meaningful
            information.</p>

        <p>
            <a class="btn btn-primary" href="https://gitlab.com/davidcochrum/gridium-php/blob/master/index.php"
               target="_blank">
                <i class="fas fa-code"></i> Marvel at the source</a>
            <a class="btn btn-primary" href="seven-day.php">
                <i class="fas fa-code-branch"></i> 7 Day Tide Table Implementation</a>
        </p>

        <div class="clearfix"></div>
    </div>

    <?php
    $client = new Client();
    foreach ($locations as $location) :
        // Extract the location data.
        ['slug' => $slug, 'city' => $city] = $location;

        // Initialize arrays to contain the scraped data.
        /** @var DateTimeImmutable[] $dates */
        /** @var DateTimeImmutable[] $sunrises */
        /** @var DateTimeImmutable[] $sunsets */
        /** @var DateTimeImmutable[][] $lowTideTimes */
        /** @var float[][] $lowTideHeights */
        $dates = $rowDateMap = $sunrises = $sunsets = $lowTideTimes = $lowTideHeights = [];

        // Fetch the HTML containing the tide information.
        $url = str_replace('{location}', $slug, $urlPattern);
        $crawler = $client->request('GET', $url);

        // Iterate over each day heading to determine the number of row spanned.
        $crawler->filter('.tide-events td.date')
            ->each(function (Crawler $node) use (&$dates, &$rowDateMap) {
                $dateIndex = count($dates);
                $rawDate = trim($node->text());
                [$rawTimezone] = $node->siblings()->filter('td.time-zone')->extract(['_text']);
                $timezone = new DateTimeZone(trim($rawTimezone));

                // Add a DateTimeImmutable object to the list for the current day.
                $dates[$dateIndex] = new DateTimeImmutable($rawDate, $timezone);

                // Add the current date index to the column map as many times as the number of row spanned.
                for ($i = 0; $i < $node->attr('rowspan'); $i++) {
                    $rowDateMap[] = $dateIndex;
                }
            });

        // Iterate over each row of the tide events table to extract the data we need.
        $index = 0;
        $crawler->filter('.tide-events tr')
            ->each(function (Crawler $node) use (
                $dates,
                $rowDateMap,
                &$index,
                &$sunrises,
                &$sunsets,
                &$lowTideTimes,
                &$lowTideHeights
            ) {
                // Find the corresponding date.
                $dateIndex = $rowDateMap[$index];
                $date = $dates[$dateIndex];

                // Bump the index for the next iteration.
                $index++;

                // Extract sunrise time.
                $node->filter('td:contains("Sunrise")')
                    ->each(function (Crawler $node) use ($dateIndex, $date, &$sunrises) {
                        $siblings = $node->siblings();
                        [$time] = $siblings->filter('td.time')->extract(['_text']);
                        [$rawTimezone] = $siblings->filter('td.time-zone')->extract(['_text']);
                        $timezone = new DateTimeZone(trim($rawTimezone));
                        $sunrises[$dateIndex] = new DateTimeImmutable($date->format('Y-m-d ') . trim($time), $timezone);
                    });

                // Extract sunset time.
                $node->filter('td:contains("Sunset")')
                    ->each(function (Crawler $node) use ($dateIndex, $date, &$sunsets) {
                        $siblings = $node->siblings();
                        [$time] = $siblings->filter('td.time')->extract(['_text']);
                        [$rawTimezone] = $siblings->filter('td.time-zone')->extract(['_text']);
                        $timezone = new DateTimeZone(trim($rawTimezone));
                        $sunsets[$dateIndex] = new DateTimeImmutable($date->format('Y-m-d ') . trim($time), $timezone);
                    });

                // Extract daylight low tide times and heights.
                $node->filter('td.tide:contains("Low Tide")')
                    ->each(function (Crawler $node) use (
                        $dateIndex,
                        $date,
                        $sunrises,
                        $sunsets,
                        &$lowTideTimes,
                        &$lowTideHeights
                    ) {
                        $siblings = $node->siblings();
                        [$time] = $siblings->filter('td.time')->extract(['_text']);
                        [$rawTimezone] = $siblings->filter('td.time-zone')->extract(['_text']);
                        $timezone = new DateTimeZone(trim($rawTimezone));
                        $lowTideTimes[$dateIndex][] = new DateTimeImmutable($date->format('Y-m-d ') . trim($time), $timezone);

                        [$rawHeight] = $siblings->filter('td.level .imperial')->extract(['_text']);
                        $lowTideHeights[$dateIndex][] = floatval($rawHeight);
                    });
            });
        ?>
        <h3><?= $city ?>
            <small class="text-muted">
                <a href="<?= $url ?>" target="_blank" title="View original forecast for <?= $city ?>" data-toggle="tooltip">
                    <i class="fas fa-xs fa-external-link-alt"></i>
                    <span class="sr-only">View original forecast for <?= $city ?></span>
                </a>
            </small>
        </h3>

        <div class="table-responsive">
            <table class="table table-striped table-hover mb-5">
                <thead>
                <tr>
                    <td>Date</td>
                    <td>Sunrise</td>
                    <td>Sunset</td>
                    <td>Daylight Low Tide Time</td>
                    <td>Daylight Low Tide Height (ft)</td>
                </tr>
                </thead>
                <tbody>
                <?php
                // Generate table rows containing daylight low tide data.
                foreach ($dates as $dateIndex => $date) :
                    $sunrise = $sunrises[$dateIndex] ?? null;
                    $sunset = $sunsets[$dateIndex] ?? null;
                    $lowTimes = $lowTideTimes[$dateIndex] ?? [];
                    $lowHeights = $lowTideHeights[$dateIndex] ?? [];

                    $includeDateCols = true;
                    foreach ($lowTimes as $lowIndex => $lowTime) :
                        $lowHeight = $lowHeights[$lowIndex];

                        if ($sunrise > $lowTime || $lowTime > $sunset) {
                            // The low tide did not occur between sunrise and sunset so skip it.
                            continue;
                        }
                        ?>
                        <tr>
                            <?php
                            if ($includeDateCols) :
                                // Only include unique date columns.
                                $includeDateCols = false;
                                ?>
                                <td><?= $date->format('D M j') ?></td>
                                <td><?= $sunrise->format('g:i a T') ?></td>
                                <td><?= $sunset->format('g:i a T') ?></td>
                            <?php
                            else :
                                // For any additional low tides, don't repeat the date and sunrise and sunset times.
                                ?>
                                <td colspan="3"></td>
                            <?php
                            endif;
                            ?>
                            <td><?= $lowTime->format('g:i a T') ?></td>
                            <td><?= number_format($lowHeight, 2) ?></td>
                        </tr>
                    <?php
                    endforeach;
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    <?php
    endforeach;
    ?>
</main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
<script>$(function () { $('[data-toggle="tooltip"]').tooltip(); });</script>
</body>
</html>
