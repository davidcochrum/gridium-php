<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 *
 * Contains shared location data.
 */

$la = 'America/Los_Angeles';
$ny = 'America/New_York';
$locations = [
    ['slug' => 'Half-Moon-Bay-California', 'city' => 'Half Moon Bay, California', 'timezone' => $la],
    ['slug' => 'Huntington-Beach', 'city' => 'Huntington Beach, California', 'timezone' => $la],
    ['slug' => 'Providence-Rhode-Island', 'city' => 'Providence, Rhode Island', 'timezone' => $ny],
    ['slug' => 'Wrightsville-Beach-North-Carolina', 'city' => 'Wrightsville Beach, North Carolina', 'timezone' => $ny],
];
