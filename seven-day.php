<?php
/**
 * @author David Cochrum <davidcochrum@gmail.com>
 *
 * Showcasing my ability to scrape data from HTML tables and parse it into meaningful information.
 *
 * This implementation parses the "7 Day Tide Table" views for each of the requested cities:
 * - Half Moon Bay, California
 * - Huntington Beach, California
 * - Providence, Rhode Island
 * - Wrightsville Beach, North Carolina
 */

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

include_once 'vendor/autoload.php';

// Set the URL pattern with a placeholder for the location slug.
$urlPattern = 'https://www.tide-forecast.com/locations/{location}/forecasts/latest/six_day';

// Import the list of data for each location.
include_once '_locations.php';

$title = "David Cochrum's Gridium Test";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <title><?= $title ?></title>
</head>
<body>
<main role="main" class="container">
    <div class="jumbotron">
        <img class="img-thumbnail float-right" src="my-pretty-face.jpg" alt="My pretty face" style="max-width: 7rem;">

        <h1><?= $title ?></h1>

        <p class="lead">Showcasing my ability to scrape data from HTML tables and parse it into meaningful
            information.</p>

        <p>
            <a class="btn btn-primary" href="https://gitlab.com/davidcochrum/gridium-php/blob/master/seven-day.php"
              target="_blank">
                <i class="fas fa-code"></i> Marvel at the source</a>
            <a class="btn btn-primary" href="/">
                <i class="fas fa-code-branch"></i> Tide Times Implementation</a>
        </p>

        <div class="clearfix"></div>
    </div>

    <?php
    $client = new Client();
    foreach ($locations as $location) :
        // Extract the location data.
        ['slug' => $slug, 'city' => $city, 'timezone' => $timezone] = $location;
        $timezone = new DateTimeZone($timezone);

        // Initialize arrays to contain the scraped data.
        /** @var DateTimeImmutable[] $dates */
        /** @var DateTimeImmutable[] $sunrises */
        /** @var DateTimeImmutable[] $sunsets */
        /** @var DateTimeImmutable[][] $daylightLowTideTimes */
        /** @var float[][] $daylightLowTideHeights */
        $dates = $colDateMap = $sunrises = $sunsets = $daylightLowTideTimes = $daylightLowTideHeights = [];

        // Fetch the HTML containing the tide information.
        $url = str_replace('{location}', $slug, $urlPattern);
        $crawler = $client->request('GET', $url);

        // Initialize the date starting with today local to the location.
        $date = new DateTime('today', $timezone);

        // Iterate over each day heading to determine the number of columns spanned.
        $crawler->filter('#forecasts tr.lar.hea td')
            ->each(function (Crawler $node) use ($date, &$dates, &$colDateMap) {
                $dateIndex = count($dates);

                // Add a DateTimeImmutable object to the list for the current day.
                $dates[$dateIndex] = DateTimeImmutable::createFromMutable($date);

                // Push our date iterator to the next day.
                $date->modify('+1 day');

                // Add the current date index to the column map as many times as the number of columns spanned.
                for ($i = 0; $i < $node->attr('colspan'); $i++) {
                    $colDateMap[] = $dateIndex;
                }
            });

        // Iterate over each cell of the sunrise times row and match them to their corresponding dates.
        $index = 0;
        $crawler->filter('#forecasts th:contains("Sunrise")')->siblings()
            ->each(function (Crawler $node) use ($dates, $colDateMap, &$index, &$sunrises) {
                $dateIndex = $colDateMap[$index];
                $index++;
                if ('-' == trim($node->text())) {
                    // Skip over the empty cells.
                    return;
                }

                // Parse the sunrise time into a DateTimeImmutable object and add it to the list for the corresponding day.
                $date = $dates[$dateIndex];
                $sunrises[$dateIndex] = new DateTimeImmutable($date->format('Y-m-d ') . trim($node->text()) . ' am',
                    $date->getTimezone());
            });

        // Iterate over each cell of the sunset times row and match them to their corresponding dates.
        $index = 0;
        $crawler->filter('#forecasts th:contains("Sunset")')->siblings()
            ->each(function (Crawler $node) use ($dates, $colDateMap, &$index, &$sunsets) {
                $dateIndex = $colDateMap[$index];
                $index++;
                if ('-' == trim($node->text())) {
                    // Skip over the empty cells.
                    return;
                }

                // Parse the sunset time into a DateTimeImmutable object and add it to the list for the corresponding day.
                $date = $dates[$dateIndex];
                $sunsets[$dateIndex] = new DateTimeImmutable($date->format('Y-m-d ') . trim($node->text()) . ' pm',
                    $date->getTimezone());
            });

        // Iterate over each cell of the low tide times row and determine which occur between sunrise and sunset.
        $index = 0;
        $crawler->filter('#forecasts th:contains("Low Tide")')->siblings()
            ->each(function (Crawler $node) use (
                $colDateMap,
                $dates,
                $sunrises,
                $sunsets,
                &$index,
                &$daylightLowTideTimes,
                &$daylightLowTideHeights
            ) {
                $dateIndex = $colDateMap[$index];
                $date = $dates[$dateIndex];
                $sunrise = $sunrises[$dateIndex] ?? null;
                $sunset = $sunsets[$dateIndex] ?? null;
                $index++;
                if ('' == trim($node->text()) || !$sunrise || !$sunset) {
                    // Skip over empty columns or days without sunrise or sunset information.
                    return;
                }

                // Parse the tide height.
                $height = floatval($node->filter('.heighttide')->text());

                // Parse the low tide time into a DateTimeImmutable object.
                $lowTideTime = new DateTimeImmutable($date->format('Y-m-d ') . trim($node->filter('b')->text()),
                    $date->getTimezone());

                if ($sunrise <= $lowTideTime && $lowTideTime <= $sunset) {
                    // The low tide occurs between sunrise and sunset (during daylight) so add the low tide time and
                    // height to the list for the day (there could be more than 1).
                    $daylightLowTideTimes[$dateIndex][] = $lowTideTime;
                    $daylightLowTideHeights[$dateIndex][] = $height;
                }
            });
        ?>
        <h3><?= $city ?>
            <small class="text-muted">
                <a href="<?= $url ?>" target="_blank" title="View original forecast for <?= $city ?>" data-toggle="tooltip">
                    <i class="fas fa-xs fa-external-link-alt"></i>
                    <span class="sr-only">View original forecast for <?= $city ?></span>
                </a>
            </small>
        </h3>

        <div class="table-responsive">
            <table class="table table-striped table-hover mb-5">
                <thead>
                <tr>
                    <td>Date</td>
                    <td>Sunrise</td>
                    <td>Sunset</td>
                    <td>Daylight Low Tide Time</td>
                    <td>Daylight Low Tide Height (ft)</td>
                </tr>
                </thead>
                <tbody>
                <?php
                // Generate table rows containing daylight low tide data.
                foreach ($dates as $dateIndex => $date) :
                    $sunrise = $sunrises[$dateIndex] ?? null;
                    $sunset = $sunsets[$dateIndex] ?? null;
                    $lowTimes = $daylightLowTideTimes[$dateIndex] ?? [];
                    $lowHeights = $daylightLowTideHeights[$dateIndex] ?? [];

                    foreach ($lowTimes as $lowIndex => $lowTime) :
                        $lowHeight = $lowHeights[$lowIndex];
                        ?>
                        <tr>
                            <?php
                            if (0 == $lowIndex) :
                                // For the first low tide of the day, output the date and sunrise and sunset times.
                                ?>
                                <td><?= $date->format('D M j') ?></td>
                                <td><?= $sunrise->format('g:i a T') ?></td>
                                <td><?= $sunset->format('g:i a T') ?></td>
                            <?php
                            else :
                                // For any additional low tides, don't repeat the date and sunrise and sunset times.
                                ?>
                                <td colspan="3"></td>
                            <?php
                            endif;
                            ?>
                            <td><?= $lowTime->format('g:i a T') ?></td>
                            <td><?= number_format($lowHeight, 2) ?></td>
                        </tr>
                    <?php
                    endforeach;
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    <?php
    endforeach;
    ?>
</main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
<script>$(function () { $('[data-toggle="tooltip"]').tooltip(); });</script>
</body>
</html>
